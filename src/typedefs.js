/**
 * The semantic-release plugin config.
 * @typedef {Object} PluginConfig
 */

/**
 * The context provided by semantic-release.
 * @typedef {Object} Context
 * @property {Object} options Options passed to semantic-release via CLI, configuration files etc.
 * @property {boolean} options.debug
 */

/**
 * A plugin step.
 * @typedef {(pluginConfig: PluginConfig, context: Context) => Promise<any>} PluginStep
 */

/**
 * A plugin step decorator.
 * @typedef {(step: PluginStep) => PluginStep} PluginStepDecorator
 */

exports.unused = {};
